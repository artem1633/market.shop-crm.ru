<?php

namespace App\Http\Controllers;

use App\Contact;
use App\CsvData;
use App\Http\Requests\CsvImportRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{

    public function getImport()
    {
        return view('import');
    }

    public function parseImport(CsvImportRequest $request)
    {
        $authKey = $request->authkey;

        session_start();

        if()
        $_SESSION['authkey'] = $authKey;

        $storing = false;

        $apiVer = 'v2.1.0';

        $geo_id = 213;

        $data = array();

        $path = $request->file('csv_file')->getRealPath();

        $file = fopen($path, "r");

        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Authorization: $authKey"
            ]
        ];

        while (($row = fgetcsv($file, 0, ",")) !== FALSE) {
            //Dump out the row for the sake of clarity.
            //echo 'array:';
            array_push($data, explode(";", $row[0]));
        }

        fclose($file);

        $i = 0;

        if ($request->exportToDB == 0) {
            return view('import_fields', compact('data', 'i', 'apiVer', 'geo_id', 'storing', 'opts'));
        }

        else {

            $out = fopen("data.xls", 'w');

            foreach ($data as $row) {


                $i=-1;
                $xlrow = [];

                foreach ($row as $value) {

                    $i++;


                        switch ($i) {

                            case(0):

                                $xlrow[0] = $value;

                                break;

                            case(1):

                                array_push($xlrow, $value);

                                break;

                            case(2):

                                array_push($xlrow, $value);

                                break;

                            case(3):

                                array_push($xlrow, $value);

                                break;

                            case(4):

                                $context = stream_context_create($opts);

                                $json = file_get_contents("https://api.content.market.yandex.ru/$apiVer/models/$value?geo_id=$geo_id", false, $context);

                                $obj = json_decode($json);

                                array_push($xlrow, $value);

                                array_push($xlrow, $obj->model->name);

                                array_push($xlrow, $obj->model->description);

                                array_push($xlrow, $obj->model->photo->url);

                                break;

                        }



                    if($i == 4) {
                        fputcsv($out, $xlrow, "\t");
                    }
                }

            }

            fclose($out);
            return view('import_success');
        }

    }


    public function processImport(Request $request)
{
    $data = CsvData::find($request->csv_data_file_id);
    $csv_data = json_decode($data->csv_data, true);
    foreach ($csv_data as $row) {
        $contact = new Contact();
        foreach (config('app.db_fields') as $index => $field) {
            if ($data->csv_header) {
                $contact->$field = $row[$request->fields[$field]];
            } else {
                $contact->$field = $row[$request->fields[$index]];
            }
        }
        $contact->save();
    }

    return view('import_success');
}

}
